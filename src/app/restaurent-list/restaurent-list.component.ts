import { Component, OnInit } from '@angular/core';
import { Restaurants } from '../restaurent-data';
import { StarRatingComponent } from 'ng-starrating';

@Component({
  selector: 'app-restaurent-list',
  templateUrl: './restaurent-list.component.html',
  styleUrls: ['./restaurent-list.component.scss']
})
export class RestaurentListComponent implements OnInit {

  restaurantsData: any
  searchedKeyword: any;
  grid = true;
  constructor() {
    this.restaurantsData = Restaurants  //i've used static data to fetch books data please check data.ts
    console.log(this.restaurantsData)
  }
  toggleBtn = true
  ngOnInit(): void {
  }
  toggleBtnFunc(){
  
    this.toggleBtn = !this.toggleBtn;
  }
  selectedResto: any;

  onSelect(resto: any): void {
    this.selectedResto = resto;
  }

}
