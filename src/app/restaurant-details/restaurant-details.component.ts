import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Restaurants } from '../restaurent-data';

@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.scss']
})
export class RestaurantDetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute
  ) { }
  Restaurants: any
  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const restoIdFromRoute = Number(routeParams.get('restaurantId'));
    this.Restaurants = Restaurants.find(restaurant => restaurant.id === restoIdFromRoute);

  }

}
