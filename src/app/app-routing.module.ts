import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { RestaurentListComponent } from './restaurent-list/restaurent-list.component';

const routes: Routes = [
  {
    path: '',
    component: RestaurentListComponent
  },
  { path: 'restaurant/:restaurantId', component: RestaurantDetailsComponent },

  {
    path: '**',
    redirectTo: '' //any other path will redirect to default path that is blank path or route we can redirect to 404 page not found also
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {

    useHash: true,

  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
